﻿using Assets.Scripts.AssetLoading;
using Assets.Scripts.GameStates;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Installers
{
    public class LoadingSceneInstaller : MonoInstaller
    {
        [SerializeField] private AssetLoadingController _assetLoadingController;

        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<AssetLoadingController>().FromInstance(_assetLoadingController);
            Container.Bind<GameStateController>().AsSingle();
        }
    }
}