using Assets.Scripts.GameplayConfig;
using Assets.Scripts.HighScores;
using Assets.Scripts.LevelFlowControllers;
using Assets.Scripts.LevelFlowControllers.States;
using Assets.Scripts.LevelSystems;
using Assets.Scripts.Player;
using Assets.Scripts.Ui;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Installers
{

    public class GameSceneInstaller : MonoInstaller
    {
        [SerializeField] private PlayerMovementController _playerMovementController;
        [SerializeField] private PlayerCollisionController _playerCollisionController;

        [SerializeField] private PlayerConfig _playerConfig;
        [SerializeField] private PlayerLevelConfig _playerLevelConfig;
        [SerializeField] private LevelConfig _levelConfig;
        [SerializeField] private ObstaclesConfig _obstaclesConfig;
        [SerializeField] private ObstaclesMoveConfig _obstaclesMoveConfig;

        [SerializeField] private CountdownView _countdownView;
        [SerializeField] private PlayerDiedView _playerDiedView;
        [SerializeField] private InGameHighScorePanelView _inGameHighScorePanelView;

        public override void InstallBindings()
        {
#if UNITY_EDITOR
            Container.BindInterfacesTo<PcInputSystem>().AsSingle();
#else
            Container.BindInterfacesTo<MobileInputSystem>().AsSingle();
#endif

            Container.Bind<PlayerHealthController>().AsSingle();
            Container.BindInterfacesTo<LevelFlowController>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<HighScoreController>().AsSingle();
            Container.BindInterfacesAndSelfTo<ObstaclesSpawnController>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<ObstaclesMoveSystem>().AsSingle();
            Container.BindInterfacesTo<ObstaclesPoolSystem>().AsSingle().NonLazy();

            Container.Bind<PlayerCollisionController>().FromInstance(_playerCollisionController);
            Container.BindInterfacesTo<PlayerMovementController>().FromInstance(_playerMovementController);
            
            InstallConfigsBindings();
            InstallViewBindings();
            InstallStatesBindings();
        }

        private void InstallConfigsBindings()
        {
            Container.BindInterfacesTo<PlayerConfig>().FromInstance(_playerConfig);
            Container.BindInterfacesTo<LevelConfig>().FromInstance(_levelConfig);
            Container.Bind<PlayerLevelConfig>().FromInstance(_playerLevelConfig);
            Container.Bind<ObstaclesConfig>().FromInstance(_obstaclesConfig);
            Container.Bind<ObstaclesMoveConfig>().FromInstance(_obstaclesMoveConfig);
        }

        private void InstallViewBindings()
        {
            Container.Bind<CountdownView>().FromInstance(_countdownView);
            Container.Bind<PlayerDiedView>().FromInstance(_playerDiedView);
            Container.Bind<InGameHighScorePanelView>().FromInstance(_inGameHighScorePanelView);
        }

        private void InstallStatesBindings()
        {
            Container.Bind<LevelCountdownState>().AsSingle();
            Container.Bind<LevelGameplayState>().AsSingle();
            Container.Bind<PlayerDiedState>().AsSingle();
        }
    }
}