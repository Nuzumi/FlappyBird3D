﻿using Assets.Scripts.Ui;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Installers
{
    public class MenuSceneInstaller : MonoInstaller
    {
        [SerializeField] private MenuView _menuView;
        [SerializeField] private HighScoresView _highScoresView;

        public override void InstallBindings()
        {
            Container.BindInterfacesTo<MenuController>().AsSingle().NonLazy();

            Container.Bind<MenuView>().FromInstance(_menuView);
            Container.Bind<HighScoresView>().FromInstance(_highScoresView);
        }
    }
}