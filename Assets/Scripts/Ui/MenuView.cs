using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Ui
{
    public class MenuView : MonoBehaviour
    {
        public event Action PlayButtonClicked;
        public event Action HighScoresButtonClicked;

        [SerializeField] private Button _playButton;
        [SerializeField] private Button _highScoresButton;

        public void SetActive(bool active) => gameObject.SetActive(active);

        private void Awake()
        {
            _playButton.onClick.AddListener(OnPlayButtonClicked);
            _highScoresButton.onClick.AddListener(OnHighScoresButtonClicked);
        }

        private void OnDestroy()
        {
            _playButton.onClick.RemoveAllListeners();
            _highScoresButton.onClick.RemoveAllListeners();
        }

        private void OnPlayButtonClicked() => PlayButtonClicked?.Invoke();

        private void OnHighScoresButtonClicked() => HighScoresButtonClicked?.Invoke();
    }
}