﻿using Assets.Scripts.AssetLoading;
using System;
using Zenject;

namespace Assets.Scripts.Ui
{
    public class MenuController : IInitializable, IDisposable
    {
        private readonly MenuView _menuView;
        private readonly HighScoresView _highScoresView;
        private readonly AssetLoadingController _assetLoadingController;

        public MenuController(MenuView menuView, HighScoresView highScoresView, AssetLoadingController assetLoadingController)
        {
            _menuView = menuView;
            _highScoresView = highScoresView;
            _assetLoadingController = assetLoadingController;

            Subscribe();
        }

        public void Initialize() => _menuView.SetActive(true);
        
        public void Dispose()
        {
            _menuView.HighScoresButtonClicked -= OnMenuHighScoresButtonClicked;
            _menuView.PlayButtonClicked -= OnMenuPlayButtonClicked;
            _highScoresView.BackButtonClicked -= OnHighScoresBackButtonClicked;
        }

        private void Subscribe()
        {
            _menuView.HighScoresButtonClicked += OnMenuHighScoresButtonClicked;
            _menuView.PlayButtonClicked += OnMenuPlayButtonClicked;
            _highScoresView.BackButtonClicked += OnHighScoresBackButtonClicked;
        }

        private void OnHighScoresBackButtonClicked()
        {
            _menuView.SetActive(true);
            _highScoresView.SetActive(false);
        }

        private void OnMenuPlayButtonClicked() => _assetLoadingController.LoadGameAsync();

        private void OnMenuHighScoresButtonClicked()
        {
            _menuView.SetActive(false);
            _highScoresView.SetActive(true);
        }

    }
}