using Assets.Scripts.HighScores;
using TMPro;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Ui
{
    public class InGameHighScorePanelView : MonoBehaviour
    {
        private const string HighScoreFormat = "F0";

        [SerializeField] private TextMeshProUGUI _scoreText;

        private HighScoreController _highScoreController;

        [Inject]
        public void Initialize(HighScoreController highScoreController)
        {
            _highScoreController = highScoreController;
        }

        public void SetActive(bool active) => gameObject.SetActive(active);

        private void Update()
        {
            _scoreText.text = _highScoreController.Score.ToString(HighScoreFormat);
        }
    }
}