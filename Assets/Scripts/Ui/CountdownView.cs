using TMPro;
using UnityEngine;

namespace Assets.Scripts.Ui
{

    public class CountdownView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _countdownText;

        public void SetActive(bool active) => gameObject.SetActive(active);

        public void SetCountdonwText(string text) => _countdownText.text = text;
    }
}