﻿using Assets.Scripts.AssetLoading;
using Assets.Scripts.HighScores;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Assets.Scripts.Ui
{
    public class PlayerDiedView : MonoBehaviour
    {
        private const string HighScoreFormat = "F0";

        [SerializeField] private TextMeshProUGUI _highScoreText;
        [SerializeField] private Button _menuButton;

        private HighScoreController _highScoreController;
        private AssetLoadingController _loadingController;

        [Inject]
        public void Initialize(HighScoreController highScoreController, AssetLoadingController assetLoadingController)
        {
            _highScoreController = highScoreController;
            _loadingController = assetLoadingController;
        }

        private void Awake()
        {
            _menuButton.onClick.AddListener(MenuButtonClicked);
        }

        private void OnDestroy()
        {
            _menuButton.onClick.RemoveAllListeners();
        }

        public void SetActive(bool active)
        {
            gameObject.SetActive(active);

            if (active)
            {
                SetUp();
            }
        }

        private void SetUp()
        {
            _highScoreText.text = _highScoreController.Score.ToString(HighScoreFormat);
        }

        private void MenuButtonClicked() => _loadingController.LoadMenuAsync();
    }
}