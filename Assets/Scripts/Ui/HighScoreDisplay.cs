﻿using System;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.Ui
{
    public class HighScoreDisplay : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _scoreText;
        [SerializeField] private TextMeshProUGUI _dateText;

        public void Initialze(float score, DateTime dateText)
        {
            _scoreText.text = score.ToString("F0");
            _dateText.text = dateText.ToString();
        }
    }
}