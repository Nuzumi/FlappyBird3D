using Assets.Scripts.GameStates;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Assets.Scripts.Ui
{

    public class HighScoresView : MonoBehaviour
    {
        private const int HighScoresCap = 20;

        public event Action BackButtonClicked;

        [SerializeField] private Transform _highScoresDisplayParent;
        [SerializeField] private HighScoreDisplay _highScoreDisplayPrefab;
        [SerializeField] private Button _closeButton;

        private GameStateController _gameStateController;

        public void SetActive(bool active) => gameObject.SetActive(active);

        [Inject]
        public void Initialize(GameStateController gameStateController)
        {
            _gameStateController = gameStateController;
            CreateHeighScoreDisplays();
        }

        private void Awake()
        {
            _closeButton.onClick.AddListener(OnCloseButtonClicked);
        }

        private void OnDestroy()
        {
            _closeButton.onClick.RemoveAllListeners();
        }

        private void OnCloseButtonClicked() => BackButtonClicked?.Invoke();

        private void CreateHeighScoreDisplays()
        {
            var highscores = _gameStateController.HighScoresData.OrderByDescending(x => x.Score).Take(HighScoresCap);

            foreach(var highscore in highscores)
            {
                var display = Instantiate(_highScoreDisplayPrefab, _highScoresDisplayParent);
                display.Initialze(highscore.Score, highscore.Time.DateTime);
            }
        }
    }
}