﻿namespace Assets.Scripts.GameplayConfig
{
    public interface ILevelCountdownLength
    {
        int CountdownLength { get; }
    }
}