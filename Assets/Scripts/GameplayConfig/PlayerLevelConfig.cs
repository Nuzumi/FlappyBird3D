﻿using UnityEngine;

namespace Assets.Scripts.GameplayConfig
{

    [CreateAssetMenu(menuName = "Data/"+ nameof(PlayerLevelConfig), fileName = nameof(PlayerLevelConfig))]
    public class PlayerLevelConfig : ScriptableObject
    {
        [field: SerializeField] public float MinHorizontalPlayerPosition { get; private set; }
        [field: SerializeField] public float MaxHorizontalPlayerPosition { get; private set; }
    }
}