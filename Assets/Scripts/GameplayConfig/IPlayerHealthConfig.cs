﻿namespace Assets.Scripts.GameplayConfig
{
    public interface IPlayerHealthConfig
    {
        int PlayerHealth { get; }
    }
}