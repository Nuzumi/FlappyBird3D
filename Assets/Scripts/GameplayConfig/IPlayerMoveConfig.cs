﻿namespace Assets.Scripts.GameplayConfig
{
    public interface IPlayerMoveConfig
    {
        float JumpStrength { get; }
        float HorizontalMoveSpeed { get; }
        float MaxVerticalPosition { get; }
    }
}