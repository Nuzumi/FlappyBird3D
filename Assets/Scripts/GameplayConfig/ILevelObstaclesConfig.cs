﻿using UnityEngine;

namespace Assets.Scripts.GameplayConfig
{
    public interface ILevelObstaclesConfig
    {
        int ObstaclesAmount { get; }
        int ActiveObstaclesAmount { get; }
        Vector3 ObstaclesStartingPoint { get; }
        Vector3 ObstaclesOffset { get; }
        Vector3 ObstaclePassedPosition { get; }
    }
}