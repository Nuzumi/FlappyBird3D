using UnityEngine;

namespace Assets.Scripts.GameplayConfig
{


    [CreateAssetMenu(menuName = "Data/" + nameof(PlayerConfig), fileName = nameof(PlayerConfig))]
    public class PlayerConfig : ScriptableObject, IPlayerMoveConfig, IPlayerHealthConfig
    {
        [field: SerializeField] public float JumpStrength { get; private set; }
        [field: SerializeField] public float HorizontalMoveSpeed { get; private set; }
        [field: SerializeField] public float MaxVerticalPosition { get; private set; }
        [field: SerializeField] public int PlayerHealth { get; private set; }

    }
}