﻿using UnityEngine;

namespace Assets.Scripts.GameplayConfig
{

    [CreateAssetMenu(menuName = "Data/" + nameof(LevelConfig), fileName = nameof(LevelConfig))]
    public class LevelConfig : ScriptableObject, ILevelObstaclesConfig, ILevelCountdownLength
    {
        [field: SerializeField] public int ObstaclesAmount { get; private set; }
        [field: SerializeField] public int ActiveObstaclesAmount { get; private set; }
        [field: SerializeField] public Vector3 ObstaclesStartingPoint { get; private set; }
        [field: SerializeField] public Vector3 ObstaclesOffset { get; private set; }
        [field: SerializeField] public Vector3 ObstaclePassedPosition { get; private set; }
        [field: SerializeField] public int CountdownLength { get; private set; }

        private void OnValidate()
        {
            if(ObstaclesAmount < ActiveObstaclesAmount)
            {
                Debug.LogError($"{nameof(ObstaclesAmount)} must be greater or equalt to {nameof(ActiveObstaclesAmount)}");
            }
        }
    }
}