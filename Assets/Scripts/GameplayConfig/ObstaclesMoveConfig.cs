﻿using UnityEngine;

namespace Assets.Scripts.GameplayConfig
{
    [CreateAssetMenu(menuName = "Data/" + nameof(ObstaclesMoveConfig), fileName = nameof(ObstaclesMoveConfig))]
    public class ObstaclesMoveConfig : ScriptableObject
    {
        [field: SerializeField] public Vector3 ObstacleMoveVector {  get; private set; }
        [field: SerializeField] public Vector3 SpeedUpVector { get; private set; }
        [field: SerializeField] public int SpeedUpObstacleCount { get; private set; }
    }
}