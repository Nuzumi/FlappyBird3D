﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.GameplayConfig
{
    [CreateAssetMenu(menuName = "Data/" + nameof(ObstaclesConfig), fileName = nameof(ObstaclesConfig))]
    public class ObstaclesConfig : ScriptableObject
    {
        [SerializeField] private List<ObstacleData> obstaclesData;

        private int _weightSum;

        public GameObject GetRandomObstaclePrefab()
        {
            var random = UnityEngine.Random.Range(0, _weightSum - 1);

            foreach(var obstacleData in obstaclesData)
            {
                if(random < obstacleData.weight)
                {
                    return obstacleData.Prefab;
                }

                random -= obstacleData.weight;
            }

            throw new Exception("No obstacle selected, sth went wrong");
        }

        private void OnEnable()
        {
            _weightSum = obstaclesData.Sum(data => data.weight);
        }

        [Serializable]
        private class ObstacleData
        {
            public GameObject Prefab;
            public int weight;
        }
    }
}