using Assets.Scripts.GameplayConfig;
using System;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.LevelSystems
{
    public class ObstaclesMoveSystem : ITickable, IInitializable, IDisposable
    {
        private readonly ObstaclesSpawnController _obstaclesSpawnController;
        private readonly ObstaclesMoveConfig _obstalcesMoveConfig;
        private readonly IPassedObstacleCounter _passedObstacleCounter;

        private bool _isActive;
        private Vector3 _currentMoveVector;

        public ObstaclesMoveSystem(ObstaclesSpawnController obstaclesSpawnController, ObstaclesMoveConfig obstalcesMoveConfig, IPassedObstacleCounter passedObstacleCounter)
        {
            _obstaclesSpawnController = obstaclesSpawnController;
            _obstalcesMoveConfig = obstalcesMoveConfig;
            _passedObstacleCounter = passedObstacleCounter;
        }

        public void Initialize()
        {
            _currentMoveVector = _obstalcesMoveConfig.ObstacleMoveVector;
            _passedObstacleCounter.ObstacleCountChanged += OnPassedObstacleCountChanged;
        }

        public void Dispose()
        {
            _passedObstacleCounter.ObstacleCountChanged -= OnPassedObstacleCountChanged;
        }

        public void SetActive(bool active) => _isActive = active;

        public void Tick()
        {
            if (!_isActive)
            {
                return;
            }

            foreach(var obstacle in _obstaclesSpawnController.ActiveObstacles)
            {
                obstacle.transform.position += _currentMoveVector * Time.deltaTime;
            }
        }

        private void OnPassedObstacleCountChanged()
        {
            if(_obstalcesMoveConfig.SpeedUpObstacleCount == 0)
            {
                return;
            }

            var passedObstacles = _passedObstacleCounter.PassedObstacleCount;
            var speedUpCount = passedObstacles / _obstalcesMoveConfig.SpeedUpObstacleCount;
            _currentMoveVector = _obstalcesMoveConfig.ObstacleMoveVector + _obstalcesMoveConfig.SpeedUpVector * speedUpCount;
        }
    }
}