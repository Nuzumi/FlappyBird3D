using Assets.Scripts.GameplayConfig;
using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.LevelSystems
{

    public class ObstaclesSpawnController : IInitializable, IDisposable
    {
        public List<GameObject> ActiveObstacles { get; } = new();
        public List<GameObject> InactiveObstacles { get; } = new();

        private readonly ObstaclesConfig _obstaclesConfig;
        private readonly ILevelObstaclesConfig _levelConfig;

        public ObstaclesSpawnController(ObstaclesConfig obstaclesConfig, ILevelObstaclesConfig levelConfig)
        {
            _obstaclesConfig = obstaclesConfig;
            _levelConfig = levelConfig;
        }

        public void Initialize()
        {
            SpawnAllObstacles();
        }

        public void Dispose()
        {
            DestroyGameObjects(InactiveObstacles);
            DestroyGameObjects(ActiveObstacles);
        }

        private void SpawnAllObstacles()
        {
            for(int i = 0; i < _levelConfig.ObstaclesAmount; i++)
            {
                var prefab = _obstaclesConfig.GetRandomObstaclePrefab();
                var spawnPosition = _levelConfig.ObstaclesStartingPoint + _levelConfig.ObstaclesOffset * i;
                var obstacle = UnityEngine.Object.Instantiate(prefab, spawnPosition, prefab.transform.rotation);

                if(_levelConfig.ActiveObstaclesAmount > i)
                {
                    ActiveObstacles.Add(obstacle);
                }
                else
                {
                    obstacle.SetActive(false);
                    InactiveObstacles.Add(obstacle);
                }
            }
        }

        private void DestroyGameObjects(List<GameObject> gameObjects)
        {
            foreach(var go in gameObjects)
            {
                UnityEngine.Object.Destroy(go);
            }
        }
    }
}