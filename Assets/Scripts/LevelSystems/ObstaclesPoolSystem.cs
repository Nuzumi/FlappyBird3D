﻿using Assets.Scripts.GameplayConfig;
using System;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.LevelSystems
{
    public interface IPassedObstacleCounter
    {
        event Action ObstacleCountChanged;
        int PassedObstacleCount { get; }
    }

    public class ObstaclesPoolSystem : ITickable, IPassedObstacleCounter
    {
        private const float OppositeDirectionsDotProduct = -1;

        public event Action ObstacleCountChanged;
        public int PassedObstacleCount { get; private set; }

        private readonly ObstaclesSpawnController _obstaclesSpawnController;
        private readonly ILevelObstaclesConfig _levelConfig;
        private readonly ObstaclesMoveConfig _obstaclesMoveConfig;

        public ObstaclesPoolSystem(ObstaclesSpawnController obstaclesSpawnController, ILevelObstaclesConfig levelConfig, ObstaclesMoveConfig obstaclesMoveConfig)
        {
            _obstaclesSpawnController = obstaclesSpawnController;
            _levelConfig = levelConfig;
            _obstaclesMoveConfig = obstaclesMoveConfig;
        }

        public void Tick()
        {
            if (_obstaclesSpawnController.ActiveObstacles.Count == 0)
            {
                return;
            }

            var activeObstacles = _obstaclesSpawnController.ActiveObstacles;
            var inactiveObstacles = _obstaclesSpawnController.InactiveObstacles;
            var firstObstacle = activeObstacles[0];

            if (!HasPassedThreshold(firstObstacle.transform.position))
            {
                return;
            }

            PassedObstacleCount++;
            ObstacleCountChanged?.Invoke();

            DisableObstacle(inactiveObstacles, firstObstacle);
            var randomObstacle = GetRandomInactiveObstacle(inactiveObstacles);
            EnableObstacle(activeObstacles, randomObstacle);
        }

        private bool HasPassedThreshold(Vector3 position)
        {
            var directionToThresholdPosition = math.normalize(((float3)(_levelConfig.ObstaclePassedPosition - position)).xz);
            var dot = math.dot(directionToThresholdPosition, math.normalize(((float3)(_obstaclesMoveConfig.ObstacleMoveVector))).xz);
            return dot == OppositeDirectionsDotProduct;
        }

        private void DisableObstacle(List<GameObject> inactiveObstacles, GameObject firstObstacle)
        {
            _obstaclesSpawnController.ActiveObstacles.RemoveAt(0);
            inactiveObstacles.Add(firstObstacle);
            firstObstacle.SetActive(false);
        }

        private static GameObject GetRandomInactiveObstacle(List<GameObject> inactiveObstacles)
        {
            var randomObstacleIdx = UnityEngine.Random.Range(0, inactiveObstacles.Count);
            var randomObstacle = inactiveObstacles[randomObstacleIdx];
            inactiveObstacles.RemoveAt(randomObstacleIdx);
            return randomObstacle;
        }

        private void EnableObstacle(List<GameObject> activeObstacles, GameObject randomObstacle)
        {
            var lastActiveObstaclePosition = activeObstacles[^1].transform.position;
            randomObstacle.transform.position = lastActiveObstaclePosition + _levelConfig.ObstaclesOffset;
            randomObstacle.SetActive(true);
            activeObstacles.Add(randomObstacle);
        }
    }
}