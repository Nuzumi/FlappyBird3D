using UnityEngine;
using Zenject;

namespace Assets.Scripts.HighScores
{
    public class HighScoreController : ITickable
    {
        public float Score { get; private set; }

        private bool _isActive;

        public void Tick()
        {
            if (!_isActive)
            {
                return;
            }

            Score += Time.deltaTime;
        }

        public void SetActive(bool active) => _isActive = active;
    }
}