using System;
using UnityEngine.InputSystem;

namespace Assets.Scripts.Player
{

    public class PcInputSystem : IDisposable, IPlayerInput
    {
        public event Action Jump;
        public float HorizontalMovement { get; private set; }

        private GameControlls.PcPlayerActions _playerActions;

        public PcInputSystem()
        {
            _playerActions = new GameControlls().PcPlayer;
            _playerActions.HorizontalMovement.performed += OnHorizontalMovementChanged;
            _playerActions.HorizontalMovement.canceled += OnHorizontalMovementChanged;
            _playerActions.Jump.performed += OnJumpPerformed;
        }

        public void Dispose()
        {
            _playerActions.Disable();
        }

        public void SetActive(bool active)
        {
            if (active)
            {
                _playerActions.Enable();
            }
            else
            {
                _playerActions.Disable();
            }
        }

        private void OnHorizontalMovementChanged(InputAction.CallbackContext context) => HorizontalMovement = context.ReadValue<float>();

        private void OnJumpPerformed(InputAction.CallbackContext _) => Jump?.Invoke();
    }
}