﻿using Assets.Scripts.LevelSystems.Obstacles;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Player
{

    public class PlayerCollisionController : MonoBehaviour
    {
        private PlayerHealthController _playerHealthController;

        [Inject]
        public void Initialize(PlayerHealthController playerHealthController)
        {
            _playerHealthController = playerHealthController;
        }

        private void OnTriggerEnter(Collider other)
        {
            if(!other.gameObject.TryGetComponent<IObstacle>(out var _))
            {
                return;
            }

            _playerHealthController.ObstacleHit();
        }
    }
}
