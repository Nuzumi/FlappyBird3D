﻿using Assets.Scripts.GameplayConfig;

namespace Assets.Scripts.Player
{
    public class PlayerHealthController
    {
        public bool IsPlayerAlive => _playerCurrenthealth > 0;

        private int _playerCurrenthealth;

        public PlayerHealthController(IPlayerHealthConfig playerHealthConfig)
        {
            _playerCurrenthealth = playerHealthConfig.PlayerHealth;
        }

        public void ObstacleHit()
        {
            _playerCurrenthealth--;
        }
    }
}
