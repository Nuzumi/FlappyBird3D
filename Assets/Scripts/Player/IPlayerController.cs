﻿namespace Assets.Scripts.Player
{
    public interface IPlayerController
    {
        void SetActive(bool active);
    }
}
