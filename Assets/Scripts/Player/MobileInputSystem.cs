﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

namespace Assets.Scripts.Player
{
    public class MobileInputSystem : IInitializable, IDisposable, IPlayerInput
    {
        private const float GoLeftMaxPercent = .25f;
        private const float GoRightMinPercent = .75f;

        public float HorizontalMovement { get; private set; }

        public event Action Jump;

        private GameControlls.MobilePlayerActions _playerActions;
        private Camera _camera;
        private bool _hasJumped;

        public MobileInputSystem()
        {
            _playerActions = new GameControlls().MobilePlayer;
            _playerActions.Press.performed += OnTouchPerformed;
            _playerActions.Press.canceled += OnTouchCanceled;
        }

        public void Initialize()
        {
            _camera = Camera.main;
        }

        public void Dispose()
        {
            _playerActions.Disable();
        }

        private void OnTouchPerformed(InputAction.CallbackContext _)
        {
            var horizontalPosition = _playerActions.Touch.ReadValue<Vector2>().x;
            var horizontalPercent = horizontalPosition / _camera.pixelWidth;

            if(horizontalPercent < GoLeftMaxPercent)
            {
                HorizontalMovement = -1;
                return;
            }

            if(horizontalPercent > GoRightMinPercent)
            {
                HorizontalMovement = 1;
                return;
            }

            if (_hasJumped)
            {
                return;
            }

            Jump?.Invoke();
            _hasJumped = true;
        }

        private void OnTouchCanceled(InputAction.CallbackContext _)
        {
            _hasJumped = false;
            HorizontalMovement = 0;
        }

        public void SetActive(bool active)
        {
            if (active)
            {
                _playerActions.Enable();
            }
            else
            {
                _playerActions.Disable();
            }
        }
    }
}