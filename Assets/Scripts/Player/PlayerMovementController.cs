﻿using Assets.Scripts.GameplayConfig;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Player
{
    public class PlayerMovementController : MonoBehaviour, IPlayerController
    {
        [SerializeField] private Rigidbody _rigidbody;

        private IPlayerInput _input;
        private IPlayerMoveConfig _playerConfig;
        private PlayerLevelConfig _levelConfig;
        private bool _isActive;

        [Inject]
        public void Initialise(IPlayerInput input, IPlayerMoveConfig playerConfig, PlayerLevelConfig levelConfig)
        {
            _input = input;
            _playerConfig = playerConfig;
            _levelConfig = levelConfig;
            _input.Jump += OnJump;
        }

        public void SetActive(bool active)
        {
            _isActive = active;
            _rigidbody.useGravity = active;
        }

        private void OnJump()
        {
            if (!_isActive)
            {
                return;
            }

            _rigidbody.AddForce(Vector3.up * _playerConfig.JumpStrength);
        }

        private void Update()
        {
            if (!_isActive)
            {
                return;
            }

            if (!CanMoveHorizontally())
            {
                return;
            }

            var horizontalMove = _input.HorizontalMovement * (Time.deltaTime * _playerConfig.HorizontalMoveSpeed);
            transform.position += new Vector3(horizontalMove, 0, 0);

            ClipPlayerVerticalPosition();
        }

        private bool CanMoveHorizontally()
        {
            if (_input.HorizontalMovement > 0 && transform.position.x >= _levelConfig.MaxHorizontalPlayerPosition)
            {
                return false;
            }

            if (_input.HorizontalMovement < 0 && transform.position.x <= _levelConfig.MinHorizontalPlayerPosition)
            {
                return false;
            }

            return true;
        }

        private void ClipPlayerVerticalPosition()
        {
            if (transform.position.y <= _playerConfig.MaxVerticalPosition)
            {
                return;
            }

            var position = transform.position;
            position.y = _playerConfig.MaxVerticalPosition;
            transform.position = position;

            var velocity = _rigidbody.velocity;
            velocity.y = 0;
            _rigidbody.velocity = velocity;
        }
    }
}
