﻿using System;

namespace Assets.Scripts.Player
{
    public interface IPlayerInput
    {
        event Action Jump;
        float HorizontalMovement { get; }

        void SetActive(bool active);
    }
}