﻿using System;
using UnityEngine;

namespace Assets.Scripts.GameStates
{
    [Serializable]
    public class HighScoreRecord
    {
        public DateTimeOffset Time
        {
            get => DateTimeOffset.FromUnixTimeMilliseconds(_timecode);
            set => _timecode = value.ToUnixTimeMilliseconds();
        }

        public float Score;
        [SerializeField] long _timecode;
    }
}
