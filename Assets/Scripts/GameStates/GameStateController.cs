﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Assets.Scripts.GameStates
{
    public class GameStateController
    {
        private const string GameStateFileName = "GameState.data";

        public IReadOnlyList<HighScoreRecord> HighScoresData => _gameState.HighScoreData;

        private GameState _gameState;

        public async void Load()
        {
            if (!File.Exists(GetGameStatePath()))
            {
                _gameState = new GameState()
                {
                    HighScoreData = new ()
                };
                return;
            }

            try
            {
                var json = await File.ReadAllTextAsync(GetGameStatePath());
                _gameState = JsonUtility.FromJson<GameState>(json);
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }
        }

        public void Save()
        {
            var json = JsonUtility.ToJson(_gameState);

            try
            {
                File.WriteAllTextAsync(GetGameStatePath(), json);
            }
            catch(Exception ex)
            {
                Debug.LogException(ex);
            }
        }

        public void AddRunScore(float score)
        {
            _gameState.HighScoreData.Add(new HighScoreRecord
            {
                Time = DateTime.Now,
                Score = score,
            });
        }

        private string GetGameStatePath() => Path.Combine(Application.persistentDataPath, GameStateFileName);
    }
}
