﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.GameStates
{
    [Serializable]
    public class GameState
    {
        public List<HighScoreRecord> HighScoreData;
    }
}
