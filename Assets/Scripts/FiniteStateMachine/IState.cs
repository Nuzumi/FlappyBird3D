﻿namespace Assets.Scripts.FiniteStateMachine
{
    public interface IState
    {
        void Tick();
        void OnEnter();
        void OnExit();
    }
}