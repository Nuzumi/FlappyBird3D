using Assets.Scripts.GameStates;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;
using Zenject;

namespace Assets.Scripts.AssetLoading
{
    public class AssetLoadingController : MonoBehaviour, IInitializable
    {
        [SerializeField] private AssetReference _gameSceneReference;
        [SerializeField] private AssetReference _menuSceneReference;

        private GameStateController _gameStateController;
        private SceneInstance _gameScene;
        private SceneInstance _menuScene;

        [Inject]
        public void Initialise(GameStateController gameStateController)
        {
            _gameStateController = gameStateController;
        }

        public async void Initialize()
        {
            _gameStateController.Load();
            await LoadMenu();
        }

        public async void LoadMenuAsync()
        {
            await SceneManager.UnloadSceneAsync(_gameScene.Scene);
            await LoadMenu();
        }

        public async void LoadGameAsync()
        {
            await SceneManager.UnloadSceneAsync(_menuScene.Scene);
            await LoadGame();
        }

        private async UniTask LoadMenu()
        {
            _menuScene = await Addressables.LoadSceneAsync(_menuSceneReference, LoadSceneMode.Additive);
        }

        private async UniTask LoadGame()
        {
            _gameScene = await Addressables.LoadSceneAsync(_gameSceneReference, LoadSceneMode.Additive);
        }
    }
}