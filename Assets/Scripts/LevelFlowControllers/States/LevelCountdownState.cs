﻿using Assets.Scripts.FiniteStateMachine;
using Assets.Scripts.GameplayConfig;
using Assets.Scripts.Ui;
using UnityEngine;

namespace Assets.Scripts.LevelFlowControllers.States
{
    public class LevelCountdownState : IState
    {
        private const int CountdonwOffset = 1;
        private const string CountdonwDisplayFormat = "F0";

        public bool HasCountdownFinished { get; private set; }

        private readonly CountdownView _countdownView;

        private float _countdown;

        public LevelCountdownState(CountdownView countdownView, ILevelCountdownLength levelCountdownLength)
        {
            _countdownView = countdownView;
            _countdown = levelCountdownLength.CountdownLength - CountdonwOffset;
        }

        public void OnEnter()
        {
            _countdownView.SetActive(true);
        }

        public void OnExit()
        {
            _countdownView.SetActive(false);
        }

        public void Tick()
        {
            _countdown -= Time.deltaTime;

            if(_countdown < 0)
            {
                HasCountdownFinished = true;
                return;
            }

            _countdownView.SetCountdonwText(_countdown.ToString(CountdonwDisplayFormat));
        }
    }
}
