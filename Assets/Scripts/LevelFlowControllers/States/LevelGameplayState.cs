﻿using Assets.Scripts.FiniteStateMachine;
using Assets.Scripts.GameStates;
using Assets.Scripts.HighScores;
using Assets.Scripts.LevelSystems;
using Assets.Scripts.Player;
using Assets.Scripts.Ui;

namespace Assets.Scripts.LevelFlowControllers.States
{
    public class LevelGameplayState : IState
    {
        private readonly ObstaclesMoveSystem _obstaclesMoveSystem;
        private readonly IPlayerController _playerController;
        private readonly HighScoreController _highScoreController;
        private readonly InGameHighScorePanelView _inGameHighScorePanelView;
        private readonly GameStateController _gameStateController;
        private readonly IPlayerInput _playerInput;

        public LevelGameplayState(ObstaclesMoveSystem obstaclesMoveSystem,
            IPlayerController playerController,
            HighScoreController highScoreController,
            InGameHighScorePanelView inGameHighScorePanelView,
            GameStateController gameStateController,
            IPlayerInput playerInput)
        {
            _obstaclesMoveSystem = obstaclesMoveSystem;
            _playerController = playerController;
            _highScoreController = highScoreController;
            _inGameHighScorePanelView = inGameHighScorePanelView;
            _gameStateController = gameStateController;
            _playerInput = playerInput;
        }

        public void OnEnter()
        {
            _obstaclesMoveSystem.SetActive(true);
            _playerController.SetActive(true);
            _highScoreController.SetActive(true);
            _inGameHighScorePanelView.SetActive(true);
            _playerInput.SetActive(true);
        }

        public void OnExit()
        {
            _obstaclesMoveSystem.SetActive(false);
            _playerController.SetActive(false);
            _highScoreController.SetActive(false);
            _inGameHighScorePanelView.SetActive(false);
            _playerInput?.SetActive(false);
            _gameStateController.AddRunScore(_highScoreController.Score);
            _gameStateController.Save();
        }

        public void Tick()
        {
        }
    }
}
