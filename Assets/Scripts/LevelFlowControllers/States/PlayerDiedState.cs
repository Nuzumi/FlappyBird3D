﻿using Assets.Scripts.FiniteStateMachine;
using Assets.Scripts.Ui;

namespace Assets.Scripts.LevelFlowControllers.States
{
    public class PlayerDiedState : IState
    {
        private readonly PlayerDiedView _playerDiedView;

        public PlayerDiedState(PlayerDiedView playerDiedView)
        {
            _playerDiedView = playerDiedView;
        }

        public void OnEnter()
        {
            _playerDiedView.SetActive(true);
        }

        public void OnExit()
        {
        }

        public void Tick()
        {
        }
    }
}
