﻿using Assets.Scripts.FiniteStateMachine;
using Assets.Scripts.LevelFlowControllers.States;
using Assets.Scripts.Player;
using Zenject;

namespace Assets.Scripts.LevelFlowControllers
{
    public class LevelFlowController : ITickable, IInitializable
    {
        private readonly StateMachine _stateMachine;
        private readonly LevelCountdownState _countdownState;
        private readonly LevelGameplayState _gameplayState;
        private readonly PlayerDiedState _playerDiedState;
        private readonly PlayerHealthController _playerHealthController;

        public LevelFlowController(
            LevelCountdownState countdownState,
            LevelGameplayState gameplayState,
            PlayerDiedState playerDiedState,
            PlayerHealthController playerHealthController)
        {
            _stateMachine = new StateMachine();
            _countdownState = countdownState;
            _gameplayState = gameplayState;
            _playerDiedState = playerDiedState;
            _playerHealthController = playerHealthController;
            SetupStateMachine();
        }

        public void Initialize()
        {
            StartGameplay();
        }

        public void StartGameplay()
        {
            _stateMachine.SetState(_countdownState);
        }

        public void Tick()
        {
            _stateMachine.Tick();
        }

        private void SetupStateMachine()
        {
            _stateMachine.AddTransition(_countdownState, _gameplayState, () => _countdownState.HasCountdownFinished);
            _stateMachine.AddTransition(_gameplayState, _playerDiedState, () => !_playerHealthController.IsPlayerAlive);
        }
    }
}
